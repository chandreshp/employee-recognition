import Vue from 'vue'
import login from './login.vue'
import Buefy from "buefy"
import "buefy/dist/buefy.css"

Vue.use(Buefy)
Vue.config.productionTip = false

new Vue({
  render: h => h(login),
}).$mount('#login')
